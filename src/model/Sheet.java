/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Flávio
 */
public class Sheet {
    
    private String url;
    private String imageUrl;
    private String imageUrlForApi;
    private String uid;
    private String name;
    private int width;
    private int height;

    public int getHeight() {
        return height;
    }

    public void setHeight(int    height) {
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getImageUrlForApi() {
        return imageUrlForApi;
    }

    public void setImageUrlForApi(String imageUrlForApi) {
        this.imageUrlForApi = imageUrlForApi;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

}
