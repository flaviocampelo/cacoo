/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package renderer;

import java.awt.Component;
import javax.swing.JTree;
import javax.swing.UIManager;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import model.Diagram;
import model.Folder;

/**
 *
 * @author Flávio
 */
public class FlavioTreeCellRenderer extends DefaultTreeCellRenderer {

    @Override
    public Component getTreeCellRendererComponent(JTree tree, Object value, boolean sel, boolean expanded, boolean leaf, int row, boolean hasFocus) {
        super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row, hasFocus);

        //customize icons from any tree item
        if (value instanceof DefaultMutableTreeNode) {
            DefaultMutableTreeNode node = (DefaultMutableTreeNode) value;
            //if folder uses the directory icon
            if (node.getUserObject() instanceof Folder) {
                setIcon(UIManager.getIcon("FileView.directoryIcon"));
            } //if diagram uses the file icon
            else if (node.getUserObject() instanceof Diagram) {
                setIcon(UIManager.getIcon("FileView.fileIcon"));
            }
        }
        return this;
    }

}
