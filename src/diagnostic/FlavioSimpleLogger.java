/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package diagnostic;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.GregorianCalendar;
import javax.swing.JOptionPane;

/**
 *
 * @author Flávio
 */
public class FlavioSimpleLogger {

    private static final String EXCEPTION_FILE = "exception.txt";

    /**
     * Append exceptions to file
     *
     * @param e exception to log
     */
    public static void logToFile(Exception e) {
        try {
            FileWriter fileWriter = new FileWriter(EXCEPTION_FILE, true);
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            PrintWriter printWriter = new PrintWriter(bufferedWriter, true);
            printWriter.println(String.format("%nEXCEPTION DATETIME:%n%tD %tT", GregorianCalendar.getInstance().getTime(), GregorianCalendar.getInstance().getTime()));
            printWriter.println(String.format("EXCEPTION MESSAGE:%n%s", e.getMessage()));
            printWriter.println("EXCEPTION STACKTRACE:");
            e.printStackTrace(printWriter);
            printWriter.close();
            bufferedWriter.close();
            fileWriter.close();
        } catch (Exception ie) {
            JOptionPane.showMessageDialog(null, "Could not write Exception to file.");
        }
    }

    /**
     * This is a demo application. Prevents a big file in the disk.
     */
    public static void removeFileLog() {
        File file = new File(EXCEPTION_FILE);
        if (file.exists()) {
            try {
                file.delete();
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, String.format("Could not remove FileLog %s.", file.getName()));
            }
        }
    }
}
