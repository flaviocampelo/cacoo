/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Flávio
 */
public class License {
    private String plan;
    private int remainingSheets;
    private int remainingSharedFolders;
    private int maxNumberOfSharersPerDiagram;
    private int maxNumberOfSharersPerSharedFolder;
    private boolean canCreateSheet;
    private boolean canCreateSharedFolder;
    private boolean canExportVectorFormat;

    public String getPlan() {
        return plan;
    }

    public void setPlan(String plan) {
        this.plan = plan;
    }

    public int getRemainingSheets() {
        return remainingSheets;
    }

    public void setRemainingSheets(int remainingSheets) {
        this.remainingSheets = remainingSheets;
    }

    public int getRemainingSharedFolders() {
        return remainingSharedFolders;
    }

    public void setRemainingSharedFolders(int remainingSharedFolders) {
        this.remainingSharedFolders = remainingSharedFolders;
    }

    public int getMaxNumberOfSharersPerDiagram() {
        return maxNumberOfSharersPerDiagram;
    }

    public void setMaxNumberOfSharersPerDiagram(int maxNumberOfSharersPerDiagram) {
        this.maxNumberOfSharersPerDiagram = maxNumberOfSharersPerDiagram;
    }

    public int getMaxNumberOfSharersPerSharedFolder() {
        return maxNumberOfSharersPerSharedFolder;
    }

    public void setMaxNumberOfSharersPerSharedFolder(int maxNumberOfSharersPerSharedFolder) {
        this.maxNumberOfSharersPerSharedFolder = maxNumberOfSharersPerSharedFolder;
    }

    public boolean isCanCreateSheet() {
        return canCreateSheet;
    }

    public void setCanCreateSheet(boolean canCreateSheet) {
        this.canCreateSheet = canCreateSheet;
    }

    public boolean isCanCreateSharedFolder() {
        return canCreateSharedFolder;
    }

    public void setCanCreateSharedFolder(boolean canCreateSharedFolder) {
        this.canCreateSharedFolder = canCreateSharedFolder;
    }

    public boolean isCanExportVectorFormat() {
        return canExportVectorFormat;
    }

    public void setCanExportVectorFormat(boolean canExportVectorFormat) {
        this.canExportVectorFormat = canExportVectorFormat;
    }
}
