/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wsclient.Serializer;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import diagnostic.FlavioSimpleLogger;
import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 *
 * @author Flávio
 */
public class FlavioDateDeserializer implements JsonDeserializer<Date> {

    private static final String[] DATE_FORMATS = new String[]{
        "EEE, dd MMM yyyy HH:mm:ss Z"
    };

    @Override
    public Date deserialize(JsonElement json, Type type, JsonDeserializationContext jdc) throws JsonParseException {
        if (json != null && json.getAsString() != null && !json.getAsString().isEmpty()) {
            for (String format : DATE_FORMATS) {
                try {
                    return new SimpleDateFormat(format, Locale.US).parse(json.getAsString());
                } catch (ParseException e) {
                    FlavioSimpleLogger.logToFile(e);
                }
            }
            throw new JsonParseException("Unsuported date format " + json.getAsString());
        }
        return null;
    }
}
