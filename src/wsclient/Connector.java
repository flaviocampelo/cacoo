/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wsclient;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import diagnostic.FlavioSimpleLogger;
import java.awt.image.BufferedImage;
import java.lang.reflect.Type;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.imageio.ImageIO;
import javax.net.ssl.HttpsURLConnection;
import javax.swing.ImageIcon;
import model.ChatMessage;
import model.Diagram;
import model.Folder;
import model.License;
import wsclient.Serializer.FlavioDateDeserializer;
import wsclient.model.ReceiveObject;

/**
 *
 * @author Flávio
 */
public class Connector {

    private static final String ROOT_URL = "https://cacoo.com/api/v1/";
    private static final int READ_TIMEOUT = 15000;

    /**
     * Get the list of existing diagrams for an account.
     *
     * @param apiKey an apiKey which represents an account.
     * @return list of existing diagrams for account.
     */
    public static List<Diagram> GetDiagrams(String apiKey) {
        List<Diagram> diagrams = new ArrayList();
        Gson gson = new GsonBuilder()
                .registerTypeAdapter(Date.class, new FlavioDateDeserializer())
                .create();
        HttpsURLConnection httpsURLConnection = null;
        ReceiveObject receiveObject = new ReceiveObject();

        try {
            URL url = new URL(String.format("%sdiagrams.json?apiKey=%s", ROOT_URL, apiKey));
            httpsURLConnection = (HttpsURLConnection) url.openConnection();
            httpsURLConnection.setRequestMethod("GET");
            httpsURLConnection.setReadTimeout(READ_TIMEOUT);
            httpsURLConnection.setDoInput(true);
            httpsURLConnection.connect();
            receiveObject.setResponseCode(httpsURLConnection.getResponseCode());
            receiveObject.setErrorMessage(httpsURLConnection.getErrorStream());
            receiveObject.setContent(httpsURLConnection.getInputStream());
            if (receiveObject.getResponseCode() >= 200 && receiveObject.getResponseCode() < 400) {
                JsonObject jsonObject = new JsonParser().parse(receiveObject.getContent()).getAsJsonObject();
                int count = jsonObject.getAsJsonPrimitive("count").getAsInt();
                //exists elements...
                if (count > 0) {
                    JsonArray jsonArray = jsonObject.getAsJsonArray("result");
                    Type type = new TypeToken<List<Diagram>>() {
                    }.getType();
                    diagrams = gson.fromJson(jsonArray, type);
                }
            }
        } catch (Exception exception) {
            FlavioSimpleLogger.logToFile(exception);
        } finally {
            try {
                //realiza a desconexão...
                if (httpsURLConnection != null) {
                    httpsURLConnection.disconnect();
                }
            } catch (Exception ex) {
                //não foi possível desconectar...
                FlavioSimpleLogger.logToFile(ex);
            }
        }
        return diagrams;
    }

    /**
     * Get the list of existing folders for an account.
     *
     * @param apiKey an apiKey which represents an account.
     * @return list of existing folders for account.
     */
    public static List<Folder> GetFolders(String apiKey) {
        List<Folder> folders = new ArrayList();
        Gson gson = new GsonBuilder()
                .registerTypeAdapter(Date.class, new FlavioDateDeserializer())
                .create();
        HttpsURLConnection httpsURLConnection = null;
        ReceiveObject receiveObject = new ReceiveObject();

        try {
            URL url = new URL(String.format("%sfolders.json?apiKey=%s", ROOT_URL, apiKey));
            httpsURLConnection = (HttpsURLConnection) url.openConnection();
            httpsURLConnection.setRequestMethod("GET");
            httpsURLConnection.setReadTimeout(READ_TIMEOUT);
            httpsURLConnection.setDoInput(true);
            httpsURLConnection.connect();
            receiveObject.setResponseCode(httpsURLConnection.getResponseCode());
            receiveObject.setErrorMessage(httpsURLConnection.getErrorStream());
            receiveObject.setContent(httpsURLConnection.getInputStream());
            if (receiveObject.getResponseCode() >= 200 && receiveObject.getResponseCode() < 400) {
                JsonObject jsonObject = new JsonParser().parse(receiveObject.getContent()).getAsJsonObject();
                JsonArray jsonArray = jsonObject.getAsJsonArray("result");
                Type type;
                type = new TypeToken<List<Folder>>() {
                }.getType();
                folders = gson.fromJson(jsonArray, type);
            }
        } catch (Exception exception) {
            FlavioSimpleLogger.logToFile(exception);
        } finally {
            try {
                //realiza a desconexão...
                if (httpsURLConnection != null) {
                    httpsURLConnection.disconnect();
                }
            } catch (Exception ex) {
                //não foi possível desconectar...
                FlavioSimpleLogger.logToFile(ex);
            }
        }
        return folders;
    }

    /**
     * Get the license info for an account.
     *
     * @param apiKey an apiKey which represents an account.
     * @return lincense info for account.
     */
    public static License GetLicense(String apiKey) {
        License license = null;
        Gson gson = new GsonBuilder()
                .create();
        HttpsURLConnection httpsURLConnection = null;
        ReceiveObject receiveObject = new ReceiveObject();
        try {
            URL url = new URL(String.format("%saccount/license.json?apiKey=%s", ROOT_URL, apiKey));
            httpsURLConnection = (HttpsURLConnection) url.openConnection();
            httpsURLConnection.setRequestMethod("GET");
            httpsURLConnection.setReadTimeout(READ_TIMEOUT);
            httpsURLConnection.setDoInput(true);
            httpsURLConnection.connect();
            receiveObject.setResponseCode(httpsURLConnection.getResponseCode());
            receiveObject.setErrorMessage(httpsURLConnection.getErrorStream());
            receiveObject.setContent(httpsURLConnection.getInputStream());
            if (receiveObject.getResponseCode() >= 200 && receiveObject.getResponseCode() < 400) {
                JsonObject jsonObject = new JsonParser().parse(receiveObject.getContent()).getAsJsonObject();
                Type type;
                type = new TypeToken<License>() {
                }.getType();
                license = gson.fromJson(jsonObject, type);
            }
        } catch (Exception exception) {
            FlavioSimpleLogger.logToFile(exception);
        } finally {
            try {
                //realiza a desconexão...
                if (httpsURLConnection != null) {
                    httpsURLConnection.disconnect();
                }
            } catch (Exception e) {
                //não foi possível desconectar...
                FlavioSimpleLogger.logToFile(e);
            }
        }
        return license;
    }

    /**
     * Get the image of the specified diagram.
     *
     * @param apiKey an apiKey which represents an account.
     * @param diagramId the unique id of the diagram.
     * @return the image of the diagram
     */
    public static ImageIcon GetDiagramImage(String apiKey, String diagramId) {
        String url = String.format("%sdiagrams/%s.png?apiKey=%s", ROOT_URL, diagramId, apiKey);
        return GetImageFromURL(url);
    }

    public static ImageIcon GetImageFromURL(String imageUrl) {
        ImageIcon imageIcon = null;
        if (imageUrl != null && !"".equals(imageUrl)) {
            try {
                URL url = new URL(imageUrl);
                BufferedImage bufferedImage = ImageIO.read(url);
                imageIcon = new ImageIcon(bufferedImage);
            } catch (Exception exception) {
                FlavioSimpleLogger.logToFile(exception);
            }
        }
        return imageIcon;
    }

    /**
     * Get the chat messages of the specified diagram.
     *
     * @param apiKey an apiKey which represents an account.
     * @param diagramId the unique id of the diagram.
     * @return The list with chat messages
     */
    public static List<ChatMessage> GetChatMessages(String apiKey, String diagramId) {
        List<ChatMessage> chatMessages = null;
        Gson gson = new GsonBuilder()
                .registerTypeAdapter(Date.class, new FlavioDateDeserializer())
                .create();
        HttpsURLConnection httpsURLConnection = null;
        ReceiveObject receiveObject = new ReceiveObject();
        try {
            URL url = new URL(String.format("%sdiagrams/%s/chat/messages.json?apiKey=%s", ROOT_URL, diagramId, apiKey));
            httpsURLConnection = (HttpsURLConnection) url.openConnection();
            httpsURLConnection.setRequestMethod("GET");
            httpsURLConnection.setReadTimeout(READ_TIMEOUT);
            httpsURLConnection.setDoInput(true);
            httpsURLConnection.connect();
            receiveObject.setResponseCode(httpsURLConnection.getResponseCode());
            receiveObject.setErrorMessage(httpsURLConnection.getErrorStream());
            receiveObject.setContent(httpsURLConnection.getInputStream());
            if (receiveObject.getResponseCode() >= 200 && receiveObject.getResponseCode() < 400) {
                JsonObject jsonObject = new JsonParser().parse(receiveObject.getContent()).getAsJsonObject();
                JsonArray jsonArray = jsonObject.getAsJsonArray("result");
                Type type = new TypeToken<List<ChatMessage>>() {
                }.getType();
                chatMessages = gson.fromJson(jsonArray, type);
            }
        } catch (Exception exception) {
            FlavioSimpleLogger.logToFile(exception);
        } finally {
            try {
                //realiza a desconexão...
                if (httpsURLConnection != null) {
                    httpsURLConnection.disconnect();
                }
            } catch (Exception e) {
                //não foi possível desconectar...
                FlavioSimpleLogger.logToFile(e);
            }
        }
        return chatMessages;
    }

    public static ReceiveObject DeleteDiagram(String apiKey, String diagramId) {
        HttpsURLConnection httpsURLConnection = null;
        ReceiveObject receiveObject = new ReceiveObject();
        try {
            URL url = new URL(String.format("%sdiagrams/%s/delete.json?apiKey=%s", ROOT_URL, diagramId, apiKey));
            httpsURLConnection = (HttpsURLConnection) url.openConnection();
            httpsURLConnection.setRequestMethod("GET");
            httpsURLConnection.setReadTimeout(READ_TIMEOUT);
            httpsURLConnection.setDoInput(true);
            httpsURLConnection.connect();
            receiveObject.setResponseCode(httpsURLConnection.getResponseCode());
            receiveObject.setErrorMessage(httpsURLConnection.getErrorStream());
            receiveObject.setContent(httpsURLConnection.getInputStream());
        } catch (Exception exception) {
            FlavioSimpleLogger.logToFile(exception);
        } finally {
            try {
                //realiza a desconexão...
                if (httpsURLConnection != null) {
                    httpsURLConnection.disconnect();
                }
            } catch (Exception e) {
                //não foi possível desconectar...
                FlavioSimpleLogger.logToFile(e);
            }
        }

        return receiveObject;
    }

}
