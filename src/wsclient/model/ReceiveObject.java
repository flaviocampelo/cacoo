/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wsclient.model;

import diagnostic.FlavioSimpleLogger;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 *
 * @author Flávio
 */
public class ReceiveObject {

    private int responseCode;
    private String content;
    private String errorMessage;
    private InputStream rawContent;

    public InputStream getRawContent() {
        return rawContent;
    }

    public String getContent() {
        return content;
    }

    public void setContent(InputStream inputStream) {
        rawContent = inputStream;
        content = null;
        if (inputStream != null) {
            try {
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                String line;
                StringBuffer stringBuffer = new StringBuffer("");
                while ((line = bufferedReader.readLine()) != null) {
                    stringBuffer.append(line);
                }
                content = stringBuffer.toString();
            } catch (IOException e) {
                FlavioSimpleLogger.logToFile(e);
                content = null;
            }
        }
    }

    public int getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(InputStream inputStream) {
        errorMessage = null;
        if (inputStream != null) {
            try {
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                String line;
                StringBuffer stringBuffer = new StringBuffer("");
                while ((line = bufferedReader.readLine()) != null) {
                    stringBuffer.append(line);
                }
                errorMessage = stringBuffer.toString();
            } catch (IOException e) {
                FlavioSimpleLogger.logToFile(e);
                errorMessage = null;
            }
        }
    }
}
